# Javascript

## En hier ga ik het hebben over een beetje Javascript

Met Javascript kan je je website interactief maken. Ik ga het hebben over:
- Basics
- Functions
- DOM Manipulatie
- Canvas
- ...

#### Basics
De basis van Javascript bestaat voornamelijk uit het begrijpen van wat je maakt. Het lezen van andermans programmas kan dus ook heel leerrijk zijn. Maar de basis van het zelf programmeren gaat dan toch wel over het maken van `variabelen`, `if` selecties en `for` of `while` loops. Even een voorbeeldje van een programmatje.
```
console.log("Hallo...");

var naam = "Jantje";
var leeftijd = 20;
var kostprijs = 0;

console.log(naam + " is " + leeftijd + " jaar.");

if(leeftijd < 6) {
    kostprijs = 0;
} else if(leeftijd < 10) {
    kostprijs = 10;
} else {
    kostprijs = 20;
}

console.log("De kostprijs voor " + naam +
    " is " + kostprijs + " euro.");
```

#### Functions
Functies worden voornamelijk gebruikt om code te bundelen die samen uitgevoerd moet worden. Of om aan een `Eventlistener` te koppelen. Een `Eventlistener` is eigenlijk iets waardoor je functie uitgevoerd word na een bepaalde handeling.

#### DOM Manipulatie
DOM Manipulatie kan gebruikt worden om je HTML pagina aan te passen als er verschillende acties worden uitgevoerd. Je kan gebruik maken van:
- `Document.createElement(...)` --> Het maken van een element.
- `Document.getElementByID(...)` --> Het zoeken naar een element met een ID in het HTML bestand.
- `.innerHTML` --> De HTML binnen een element aanpassen.
- `document.body.appendChild(...)` --> Een element toevoegen als kind van *body*.
- `.style.` --> Hiermee kan je de CSS van een element aanpassen.