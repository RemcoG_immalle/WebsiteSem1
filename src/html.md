# HTML

## Op deze pagina ga ik het hebben over HTML

Dit semester zijn we begonnen met HTML. Het maken van **websites**. Zelf vond ik deze lessen wel interessant. Ik wou wel weten hoe een website er achter alle grafische features eruit zag. Nu ben ik al redelijk gevorderd in het maken van website. Maar ik hoop nog veel meer te leren tijdens de lessen.

Wij hebben gehad over verschillende onderwerpen:

- Het maken van hoofdingen met `h1`, `h2` enz.
- Ook het maken van list kwam aanbod, maar hierin had je verschillend
    - `Unordered list` --> Lijsten waar niet echt een orde in zit (`<ul>`)
    - `Ordered list` --> Lijtsen waarbij alles op volgorde staat (`<ol>`)
    - In alle lijsten moeten `listitems` komen, dit moet je doen met `<li>`
- ...