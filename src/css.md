# CSS

## Hier ga ik het hebben over CSS

Na het maken van websites, moet je deze ook kunnen opmaken. Hierover ga ik wat meer vertellen over de volgend dingen:
- Basics
- CSS Selectors
- Libraries
- ..

#### Basics

Hier ga ik de meest gebruikte **CSS Elementen** even opsommen:
- `Font-size` --> Lettergrootte
- `Color` --> Kleur van de inhoud van een element
- `Background-color` --> Achtergrond kleur van een element
- `Border` --> Een element een border of kader geven
- ...

#### CSS Selectors

Om specifieke elementen op te maken kan je gebruik maken van selectors. Met behulp van deze selectors kan je elementen filteren.
- `p`, `div` --> Tags van elementen
- `#roodVierkant` --> Tags voor ID's
- `.vierkant` --> Tags voor classes
- `ul > li` --> Tags om een direct kind van ... aan te duiden

#### Libraries

Om het opmaken gemakkelijker te maken, kan je gebruik maken van *libraries*. In deze libraries zijn er al vooraf opgemaakte elementen. Waardoor je alle CSS code niet meer zelf moet schrijven en achteraf toch alles naar je eigen wensen kan aanpassen. De meestgebruikte *CSS libraries* zijn:
- Bootstrap
- Foundation
- Pure (Deze is het gemakkelijkste om mee te beginnen)
