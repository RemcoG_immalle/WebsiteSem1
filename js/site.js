﻿// Variabelen

var button1 = document.getElementById("body1");
var button2 = document.getElementById("body2");
var button3 = document.getElementById("body3");
var button4 = document.getElementById("body4")
var nav = document.getElementById("nav");

// Button 1

button1.addEventListener("click", function color1() {
    document.body.style.backgroundColor = "#7FFFD4";
    localStorage.setItem("backgroundColor", "#7FFFD4");
})

button1.addEventListener("click", function color2() {
    nav.style.backgroundColor = "#66cdaa";
    localStorage.setItem("navColor", "#66cdaa");
})

// Button 2

button2.addEventListener("click", function color3() {
    document.body.style.backgroundColor = "#F4A460";
    localStorage.setItem("backgroundColor", "#F4A460");
})

button2.addEventListener("click", function color4() {
    nav.style.backgroundColor = "#EE7600";
    localStorage.setItem("navColor", "#EE7600");
})

// Button 3

button3.addEventListener("click", function color5() {
    document.body.style.backgroundColor = "#f5f1de";
    localStorage.setItem("backgroundColor", "#f5f1de");
})

button3.addEventListener("click", function color6() {
    nav.style.backgroundColor = "#deb887";
    localStorage.setItem("navColor", "#deb887");
})

// Button 4

button4.addEventListener("click", function color7() {
    document.body.style.backgroundColor = "#656565";
    localStorage.setItem("backgroundColor", "#656565");
})

button4.addEventListener("click", function color8() {
    nav.style.backgroundColor = "#000000";
    localStorage.setItem("navColor", "#000000");
})

// PrevBg

function prevBg() {
    nav.style.backgroundColor = localStorage.getItem("navColor");
    document.body.style.backgroundColor = localStorage.getItem("backgroundColor");
}
prevBg();